package com.atlassian.tutorial.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.plugin.webresource.WebResourceManager;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MyServlet extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(MyServlet.class);
	private final TemplateRenderer templateRenderer;

	private final WebResourceManager webResourceManager;

	public MyServlet(TemplateRenderer templateRenderer, WebResourceManager webResourceManager) {
		this.templateRenderer = templateRenderer;
		this.webResourceManager = webResourceManager;
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
	  this.webResourceManager.requireResource("com.atlassian.tutorial.helloAui:helloAui-resources");
	  resp.setContentType("text/html;charset=utf-8");
	  templateRenderer.render("helloaui.vm", resp.getWriter());
	}
}